/**
 * Created by paulex10 on 22/12/2015.
 */
var elixir = require('laravel-elixir');

elixir(function(portal){
    portal.styles([
        'index.css',
        'portal.css',
        'bootstrap.min.css',
        'components-md.css',
        'font-awesome.min.css',
        'google-fonts.css',
        'layout.css',
        'uniform.default.css'
    ], 'public/build/css/api.portal.css');
});



elixir(function(app){
    app.scriptsIn('resources/app-modules', 'public/build/js/app-main/ie.portal.js');
});


elixir(function(utility){
    utility.scripts(
        [
            'jquery-1.11.3.min.js',
            'jquery.dataTables.min.js',
            'index.js',
            'ie-portal.js',
            'dropdown.js',
            'paulex.fileup.validator.js'
        ], 'public/build/js/utility/ie.portal.utility.js');
});


elixir(function(lib){
    lib.scripts([
        'angular.min.js',
        'angular-route.min.js'
    ], 'public/build/js/app-lib/ang-lib.js', 'resources/assets/js/node_modules/angular');
});
//
elixir(function(lib){
    lib.scripts(
        [
            'ngStorage.min.js'
        ],
        'public/build/js/app-lib/storage-lib.js', 'resources/assets/js/node_modules/ngstorage');
});

elixir(function(main){
    main.scriptsIn('public/build/js/app-lib', 'public/build/js/ie.lib.main.js');
});

elixir(function(main){
    main.browserify('ie.portal.js', 'public/build/js/ie.portal.min.js', 'public/build/js/app-main');
});