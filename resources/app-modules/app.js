/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 04/08/2015.
 */
    'use strict';
var app = angular.module("application",['ngRoute','ngStorage']);

app.config(['$httpProvider','$routeProvider', function($httpProvider, $routeProvider) {
    //$httpProvider.defaults.useXDomain = true;
    //delete $httpProvider.defaults.headers.common['X-Requested-With'];
    var store =window.localStorage.getItem('ngStorage-iPzmz');
    if(store!==null && store!==undefined) {
        $httpProvider.defaults.headers.common['vas-token'] = JSON.parse(store).ieTkup;
    }
}
]);


function Appl(){
    this.authCheck();
}

Appl.prototype.authCheck = function(){
    var location = window.location;
    var item = window.localStorage.getItem('ngStorage-iPzmz');
    if(location.pathname===index){
        if(item!==undefined&&item!==null){
            window.location.href = portal;
            return true;
        }
    }
    else if(location.pathname===portal){
        //check
        if(item===undefined||item===null){
            window.location.href = index;
            return false;
        }

        if(!Object.keys(item).length>2){
            window.location.href = index;
            return false;
        }
    }
};

var appz = new Appl();
appz.authCheck();