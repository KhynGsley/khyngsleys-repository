/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 * with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 02/12/2015.
 */

angular.module('application')
    .controller('DashboardController',['caseService','userService', DashboardController]);

function DashboardController(caseService, userService){
    this.caseSvc = caseService;
    this.userSvc = userService;
    this.name = this.userSvc.getUserFullName();
    this.userImage = this.userSvc.getUserImage();
    this.onLoading = false;
    this.recentCases = [];
    this.onStart();
    this.userSvc.registerComp(this);
}

DashboardController.prototype.onStart = function(){
    this.onLoading = true;
    this.caseSvc.getAccountRecentCases(this.userSvc.getUserId(), function(status, $data){
        if(status){
            this.recentCases = $data;
        }
        this.onLoading = false;
    }.bind(this));
};

DashboardController.prototype.onResume = function(){

};

DashboardController.prototype.caseColor = function(typ, roll){
    roll = roll.toLowerCase();
    if(typ===0 && roll==="open"){
        return true;
    }
    else if(typ===1 && roll==="pending"){
        return true;
    }else if(typ===3 && roll==="in_progress"){
        return true;
    }
    else return !!(typ === 2 && roll === "closed");
};