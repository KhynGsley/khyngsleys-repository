/**
 * Created by paulex10 on 24/11/2015.
 */
angular.module('application')
    .directive('profileSection', function(){
        return {
            restrict: 'E',
            templateUrl :'../resources/app-modules/Components/profile-section.directive.html'
        }
    });