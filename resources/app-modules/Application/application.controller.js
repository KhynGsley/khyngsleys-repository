/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
  * with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 14/12/2015.
 */
angular.module('application')
    .controller('ApplicationController', ['loginService','profileService','userService', ApplicationController]);


function ApplicationController(loginService, profileService, userService){
    this.loginSvc = loginService;
    this.profileSvc = profileService;
    this.userSvc = userService;
    this.onLogout = false;
    this.onActionOpen = false;
}

ApplicationController.prototype.logoutUser = function(){
    this.onLogout = true;
    if(confirm("Do you want to logout?")) {
        this.loginSvc.doLogout({}, function (status, result) {
            if (status) {
                //window.location.href = index;
            }
        });
    }
};

ApplicationController.prototype.onChangePicture = function(form){
    var self = this;
    var fileInput = document.getElementsByName('ie-customer-pic').item(0);
    var fileReader = new FileReader();
    var data = {};
    if (fileInput.files.length > 0) {
        fileReader.onloadend = function (e) {
            data['user_image']  = e.target.result.split(',')[1];
            data['account_no'] = self.userSvc.getAccountNumber();
            //$('.ie-user-fn').fadeOut('slow', function(){
                self.onActionOpen = false;
            //});
            self.userSvc.broadcastEvent('PICTURE_CHANGE', e.target.result);
            self.profileSvc.changeUserPicture(data, function(result){
               if(result){
                   userSvc.setUserImage(result['pt_user_image']);
               }else{
                   self.userSvc.broadcastEvent('PICTURE_CHANGE', userSvc.getUserImage());
               }
            });
        };
        fileReader.readAsDataURL(fileInput.files[0]);
    }
};

ApplicationController.prototype.accountSettings = function(){
    //this.onActionOpen = true;
    console.log(this.onActionOpen);
};

ApplicationController.prototype.changePicture = function(){
    this.onActionOpen = true;
    console.log(this.onActionOpen);
};