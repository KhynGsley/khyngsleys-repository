/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 * with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 16/12/2015.
 */
angular.module('application')
    .controller('TaskController', ['taskManager',TaskController]);


function TaskController(taskManager){
    this.taskPool = taskManager.getTaskPool();
}