/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 * with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 24/11/2015.
 */
angular.module('application')
    .controller('CasesController', ['caseService','userService', CasesController]);

function CasesController(caseService, userService){
    this.caseSvc = caseService;
    this.name = userService.getAccountNumber();
    closeDetailedView();
    onAttachFile();
    this.notes = [];
    this.detailed = false;
    this.createMode = false;
    this.userImage = userService.getUserImage();

    this.caseForm = {
        "name":null,
        "account_id":userService.getUserId(),
        "cstm_channel_type_c":"customer_portal",
        "cstm_complaint_category_c":null,
        "cstm_complaint_sub_category_c":null,
        "description":null,
        "priority":null,
        "status":"Open",
        "resolution":null,
        "account_no":userService.getAccountNumber()
        //"case_number":0
    };

    this.noteForm = {
        name:null,
        description:null,
        parent_id:"",
        parent_type:"Cases",
        file:null,
        filename:null
    };

    this.newForm = angular.copy(this.caseForm);
    this.newNoteForm = angular.copy(this.noteForm);

    this.processing = false;
    this.isCreatingCase = false;
    this.isLoadingNotes = false;
    this.isCreatingNote = false;
    this.selectedCaseId = "";
    $(document).ready(function() {
        init();
    });
    initializeCaseTable(userService, getPathUrl);
    userService.registerComp(this);
}

CasesController.prototype.silentReload = function(_item){
    if(!this.detailed) {
        this.detailed = true;
        this.selectedCaseId = _item.target.attributes['data-case-id'].value;
        var caseTable = $('.ie-case-table').DataTable();
        caseTable.ajax.reload(null, false);
        this.onLoadNotes(this.selectedCaseId);
    }else{
        this.detailed = false;
    }
};

CasesController.prototype.onLoadNotes = function(caseID){
    if(this.detailed) {
        this.isLoadingNotes = true;
        this.notes = [];
        angular.copy(this.newNoteForm, this.noteForm);
        this.caseSvc.getCaseNotes(caseID, function (result) {
            if (result) {
                if(this.detailed) {
                    this.notes = result;
                }
                this.isLoadingNotes = false;
            }
        }.bind(this));
    }else{
        this.notes = [];
        angular.copy(this.newNoteForm, this.noteForm);
    }
};

CasesController.prototype.triggerCreateMode = function(){
    this.createMode = !this.createMode;
    angular.copy(this.newForm, this.caseForm);
};

CasesController.prototype.createCase = function(data){
    //TODO:: check if the form is valid, do a proper check here before sending to server
    var self = this;
    this.isCreatingCase = true;
    this.caseSvc.createCase(this.caseForm, function(result){
        if(result){
            this.caseSvc.getSingleCase(result.id, function(resp){
                self.isCreatingCase = false;

                appendDataToTable(resp, result);

                self.createMode = false;
                angular.copy(self.newForm, self.caseForm);
            });
        }else {
            this.isCreatingCase = false;
        }
    }.bind(this));
};

function appendDataToTable(resp, result){
    var caseTable = $('.ie-case-table').DataTable();
    var span = document.createElement('span');
    span.classList.add('ie-case-no');
    var anchor = document.createElement("a");
    anchor.textContent = "#"+resp[0]['case_number'];
    anchor.href = "#";
    result['entry_list'].case_number = {"value":resp[0]['case_number']};
    result['entry_list'].id = {"value":resp[0]['id']};
    result['entry_list'].resolution = {"value":resp[0]['resolution']};
    span.setAttribute('data-case-id', JSON.stringify(result['entry_list']));
    span.appendChild(anchor);
    caseTable.order([['6','asc'],['0','desc']]).draw();
    caseTable.row.add(
        [
            span.outerHTML ,
            resp[0]['name'],
            resp[0]['cstm_complaint_category_c'],
            resp[0]['cstm_complaint_sub_category_c'],
            resp[0]['priority'],
            getStatusForTable(resp[0]['status']),
            resp[0]['date_entered']
        ]).draw().node();
}

/**
 * We need to convert the file to a base64 string before sending it
 * to the server..
 * When creating a case, we check if there is a file attachment,
 * its only when there is a file attachment we read the file.
 */
CasesController.prototype.createNote = function(){
    var self = this;
    var fileInput = document.getElementById('ie-file-attach');
    var fReader = new FileReader();
    this.noteForm.parent_id = self.selectedCaseId;
    this.isCreatingNote = true;
    if(fileInput.files.length>0){
        fReader.onloadend = function(e){
            self.noteForm.file = e.target.result.split(',')[1];
            self.noteForm.filename = fileInput.files[0].name;
            self.caseSvc.addNoteAttachment(self.noteForm, function(result){
                if(result){

                }else{
                    self.notes.pop();
                }
                self.isCreatingNote = false;
                angular.copy(self.newForm, self.noteForm);
            });
        };
        fReader.readAsDataURL(fileInput.files[0]);
    }else{
        this.caseSvc.addNoteAttachment(this.noteForm, function(result){
            if(result){

            }else{
                self.notes.pop();
            }
            self.isCreatingNote = false;
            angular.copy(self.newForm, self.noteForm);
        });
    }
    this.notes.push({
        description:this.noteForm.description,
        name:this.noteForm.name
    });
};