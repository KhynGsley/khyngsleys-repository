/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * @author Paul Okeke
 * Created by Okeke Paul on 24/11/2015.
 */
angular.module('application')
    .service('profileService', ['$http','$localStorage',
        'errorService','$timeout','taskManager', ProfileService]);


function ProfileService($http, $localStorage, errorService, $timeout, taskManager){
    this.http = $http;
    this.sharedPref = $localStorage;
    this.$errorSvc = errorService;
    this.tOut = $timeout;
    this.taskMgr = taskManager;
}


ProfileService.prototype.getUserDetails = function(callback){
    var self = this;
    var taskId = this.taskMgr.createTask('Initializing your profile...');
    this.http.get(getPathUrl('/user/details/'+this.sharedPref.iPzmz.ieBid, true, this.sharedPref))
        .then(function(response){
            self.$errorSvc.responseSuccess(response);
            console.log(response);
            if(response.data.status==="success") {
                callback(response.data.message);
            }else{
                callback(false);
            }
            self.taskMgr.removeTask(taskId);
    }, function(error){
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.taskMgr.removeTask(taskId);
                        self.getUserDetails(callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    callback(false);
                    self.taskMgr.removeTask(taskId);
                }
            });
    })
};


ProfileService.prototype.editUserDetails = function(userID, data, callback){
    var self = this;
    var taskId = this.taskMgr.createTask('Saving your profile details...');
    this.http.post(getPathUrl('/user/details/edit/'+userID, true, this.sharedPref), data)
        .then(function(response){
            self.$errorSvc.responseSuccess(response);
            //console.log(response);
            if(response.data.status==="success") {
                callback(response.data.message);
            }else{
                callback(false);
                console.log(response.data);
            }
            self.taskMgr.removeTask(taskId);
        }, function(error){
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.taskMgr.removeTask(taskId);
                        self.editUserDetails(userID, data, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    callback(false);
                    self.taskMgr.removeTask(taskId);
                }
            });
        })
};

ProfileService.prototype.changeUserPicture = function(data, callback){
    var self = this;
    var taskId = this.taskMgr.createTask('Uploading picture...');
    this.http.post(getPathUrl('/portal/change/picture', true, this.sharedPref), data)
        .then(function(response){
            self.$errorSvc.responseSuccess(response);
            console.log(response);
            if(response.data.status==="success") {
                callback(response.data.message);
            }else{
                console.log('failed');
                callback(false);
            }
            self.taskMgr.removeTask(taskId);
        }, function(error){
            self.$errorSvc.responseError(error, function(action){
                if(action===RETRY_REQUEST){
                    self.tOut(function(){
                        self.taskMgr.removeTask(taskId);
                        self.changeUserPicture(data, callback);
                    },5000);//wait for 5secs before trying the request
                }else if(action===REPLY_FALSE){
                    callback(false);
                    self.taskMgr.removeTask(taskId);
                }
            });
        })
};

