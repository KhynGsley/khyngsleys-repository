/* =====================================================
 *  Copyright 2015, VASCON-SOLUTIONS, Nigeria.
 *  Author Okeke Paul
 *
 *  Licensed Under VAS-CONSULTING
 *  you may not use this file or copy any of its snippet
 *  or distribute through any means except in compliance
 *  with the License.
 * ===================================================== */

/**
 * Created by paulex10 on 09/12/2015.
 */
angular.module('application')
    .service('regService', ['$http', RegService]);

function RegService($http){
    this.httpServer = $http;
}

RegService.prototype.confirmUser = function(data, callback){
    this.httpServer.post(getPathUrl('/reg/confirm', false, null),data).then(
        function(response){
            console.log(response);
            var status = response.data.status;
            if(status==="success"){
                callback(true, response.data.message);
            }else{
                callback(false, response.data.message.reason);
            }
        },
        function(error){
            console.log(error);
            this.confirmUser(data, callback);
        }.bind(this));
};



RegService.prototype.registerUser = function(data, callback){
    this.httpServer.post(getPathUrl('/reg/create/user', false, null), data).then(
        function(response){
            console.log(response);
            var status = response.data.status;
            if(status==="success"){
                callback(true, response.data.message);
            }else{
                callback(false, response.data.message.reason);
            }
        },
        function(error){
            console.log(error);
            //TODO:; if the error code is related to internet error display error message
            this.registerUser(data, callback);
        }.bind(this));
};