<?php

namespace App\Providers;

use App\CRMPortalDirectory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $modules = CRMPortalDirectory::getPath("../app/Modules");
        foreach($modules as $module)
        {
            $this->registerModuleProviders($module);
            //$this->registerRoutes($module);
        }
        if(file_exists(__DIR__.'/../Helper.php'))
        {
            require __DIR__.'/../Helper.php';
        }
    }

    public function registerModuleProviders($module)
    {
        if(file_exists(__DIR__.'/../Modules/'. $module . '/Providers/ModuleProvider.php'))
        {
            $this->app->register('App\Modules\\' . $module . '\Providers\ModuleProvider');
        }
    }
}
