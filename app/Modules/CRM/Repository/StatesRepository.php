<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 24/10/2015
 * Time: 23:30
 */

namespace App\Modules\Healthbox\Repository;


use App\Modules\Healthbox\Interfaces\StatesInterface;
use App\Modules\Healthbox\Model\State;

class StatesRepository implements StatesInterface
{

    public function __construct(State $state){
        $this->model = $state;
    }

    public function create(array $state)
    {
        return $this->model->create($state);
    }

    public function findById($id)
    {
        $state = $this->model->find($id);
        if($state!==null){
            return $state->first();
        }
        return null;
    }

    public function edit(State $state, array $data)
    {
        $state->fill($data);
        $state->save();
        return $state;
    }

    public function findByCountryId($country_id)
    {
       $states = $this->model->where('country_id',$country_id);
        if($states!=null){
            return $states->get();
        }
        return $states;
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}