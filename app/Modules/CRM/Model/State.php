<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 24/10/2015
 * Time: 23:28
 */

namespace App\Modules\Healthbox\Model;


use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    protected $fillable = ['name','country_id', 'geo_zone'];

    public function country(){
        return $this->belongsTo('App\Modules\Healthbox\Model\Country','country_id');
    }

}