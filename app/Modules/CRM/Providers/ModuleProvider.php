<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 23/04/2015
 * Time: 03:54
 */

namespace app\Modules\CRM\Providers;


use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ModuleProvider extends ServiceProvider
{

    public function boot(){
        View::addNamespace('CRM', __DIR__.'/../Views/');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}