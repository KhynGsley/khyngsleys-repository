<!--/**
* @author Okeke Paul Ugochukwu
* @company VASCON Solutions
* @company VAS-CONSULTING
* @email pugochukwu@vas-consulting.com
* @alt-email donpaul120@gmail.com
* Date: 08/12/2015
* Time: 12:15
*/-->

<!DOCTYPE html>
<html ng-app="application">
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link href="../public/assets/images/ie_logo-min.png" rel="shortcut icon" type="image/x-icon" />
    <script src="../public/build/js/ie.lib.main.js"></script>
    <title>IE Customer Portal</title>
</head>
<!--@author Okeke Paul @company VAS-CONSULTING-->
<body>
<script>
    document.getElementsByTagName('body')[0].style.display = "none";
</script>
    <div class="main">
        <header class="ie-navigation-header">
            <div class="ie-company-logo">
                <img height="80" width="80" src="../public/assets/images/ie_logo-min.png"/>
                <div class="ie-app-name">
                    <a>Ikeja Electric</a>
                    <span class="ie-app-type">Customer Portal</span>
                </div>
            </div>


            <nav>
                <ul>
                    <li>
                        <a class="nav-item" data-section="dashboard" href="#">DASHBOARD</a>
                    </li>
                    <li>
                        <a class="nav-item" data-section="cases" href="#">MY COMPLAINTS</a>
                    </li>
                    <li>
                        <a class="nav-item" data-section="faqs" href="#">FAQs</a>
                    </li>
                    <li>
                        <a class="nav-item" data-section="profile" href="#">PROFILE</a>
                    </li>
                    <li class="nav-sub-item">
                        <img class="nav-sub-item" src="../public/assets/images/sub_menu_icon.png"/>
                    </li>
                </ul>
            </nav>
        </header>

        <section ng-controller="ApplicationController as appCtrl">
        <!--We use this for the sub menus -->
        <div class="ie-sub-nav-cont">
            <div class="ie-sub-nav">
                <ul>
<!--                    <li ng-click="appCtrl.accountSettings()">Account Settings</li>-->
                    <li ng-click="appCtrl.changePicture()">Change Picture</li>
                    <li ng-click="appCtrl.logoutUser()">Logout</li>
                </ul>
            </div>
        </div>

        <!---->
            <div ng-class="{display : appCtrl.onActionOpen, noDisplay : !appCtrl.onActionOpen}" class="ie-user-fn">
                <div class="ie-user-fn-body col-md-4">
                    <div class="ie-user-fn-header ie-box-header">
                        Change Picture
                        <span ng-click="appCtrl.onActionOpen=false" style="float:right" class="ie-close-create-case">x</span>
                    </div>
                    <div class="ie-user-fn-content">
                        <div class="ie-user-fn-image">
                            <img id="ie-user-displayer" height="70" width="70"
                                 src="../public/assets/images/customer_default.jpg"/>
                        </div>
                        <form name="changePicture" ng-submit="appCtrl.onChangePicture(changePicture)" novalidate>

                            <button type="button" class="ie-browse-btn">Browse Picture</button>
                            <span id="ie-fileup-name"></span>

                            <input style="display:none" accept="image/*" type="file" name="ie-customer-pic"/>

                            <button type="submit"  class="ie-save-upload">
                                Save
                            </button>

                        </form>
                        <div class="ie-upload-note">Max upload size : 1mb </div>
                    </div>
                </div>
            </div>
        </section>
        <!--This is the main content of the page-->
        <div class="ie-content-container">
            <div class="ie-scroll">
                <dashboard-section></dashboard-section>

                <cases-section></cases-section>

                <faqs-section></faqs-section>

                <profile-section></profile-section>
            </div>
        </div>
<!--        <footer>-->
        <div ng-controller="ErrorController as errorCtrl"
             ng-class="{onError : errorCtrl.hasError(), onErrorClear : !errorCtrl.hasError()}"
             class="ie-error-container onErrorClear">
            <div class="ie-error-content">
                <img height="25" width="25" src="../public/assets/images/error_icon.png">
                <span>
                    {{errorCtrl.errorMessage()}}
                </span>
                <div class="ie-error-resolution">
                    {{errorCtrl.errorResolution()}}
                    <span ng-if="errorCtrl.hasRetry()">
                        <img height="20" width="20" src="../public/assets/images/re_connecting.gif">
                    </span>
                </div>
            </div>
        </div>
        <task-manager></task-manager>
    </div>
</body>
<script src="../public/build/js/ie.portal.min.js"></script>
<script src="../public/build/js/utility/ie.portal.utility.js"></script>
<script>
    var lateLoading = function(){
        var lnk = document.createElement('link');
        lnk.href = "http://fonts.googleapis.com/css?family=Open+Sans:100,200,400,300,600,700&subset=all";
        lnk.rel = "stylesheet";
        var head = document.getElementsByTagName('head')[0];
        head.parentNode.insertBefore(lnk, head);

        var lnk2 = document.createElement('link');
        lnk2.href = "../public/build/css/api.portal.css";
        lnk2.rel = "stylesheet";
        var head2 = document.getElementsByTagName('head')[0];
        head2.parentNode.insertBefore(lnk2, head2);
        document.getElementsByTagName('body')[0].style.display = "block";
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(lateLoading);
    else window.addEventListener('load', lateLoading);
</script>
</html>