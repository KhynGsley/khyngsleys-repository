<!--/**
* @author Okeke Paul Ugochukwu
* @company VASCON Solutions
* @company VAS-CONSULTING
* @email pugochukwu@vas-consulting.com
* @alt-email donpaul120@gmail.com
* Date: 26/02/2016
* Time: 12:15
*/-->
<html>
<head>
</head>
<body style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;">
{{--<h1 style="font-size: 25px;font-weight: 400;font-family: Tahoma" class="ie-greetings">Hi, {{$email or 'Customer'}}</h1>--}}
<div style="text-align: center">
    <img style="display: inline" src="{{url().'/assets/images/ie_logo.png'}}">
    <div style="display: inline;font-size: 20px;font-weight: bold;vertical-align: super;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;">
        Ikeja Electric</div>
</div>
<div style="background:#f9f9f9;color:#373737;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:17px;line-height:24px;max-width:100%;width:100%!important;margin:0 auto;padding:20px 0;">
    <div style="margin: 0 20%">
        <h2 style="font-size: 20px;font-weight: 600;color: #616161" class="ie-msg">
        Welcome to <span style="color: #D50000;font-size: 17px">IE Customer Portal</span>,
        </h2>

        <p style="font-size: 15px">
            We are so glad you want us to help get your issues resolved.
        </p>

        <p style="font-size: 15px;font-weight: 300;"  class="ie-msg">
            You are one step close into having a better customer service experience.
        </p>
        <p>
            Please kindly confirm your email by clicking on the link below.
            <br/>
            <i style="font-size: 13px">please note that, we may need to send you critical information about our
                service and it is important that we have an accurate email address.
            </i>
        </p>
        <script>
            function doMouseOver(obj){
                obj.style.background="#D50000";
                obj.style.boxShadow = "0 2.2px 5px rgba(10, 8, 10, 0.71)";
            }
            function doMouseOut(obj){
                obj.style.background="#C62828";
                obj.style.boxShadow = "0 2px 2px rgba(0, 0, 0, 0.5)";
            }
        </script>
        <div style="padding: 15px;text-align: center">
            <a href="{{$activation_link or ""}}" style="color: #fff;background: #C62828; padding: 20px;
            font-weight: bold;text-decoration: none;box-shadow: 0 2px 2px rgba(0, 0, 0, 0.5);"
            onmouseover="doMouseOver(this)"
            onmouseout='doMouseOut(this)'
            >
                Confirm Account
            </a>
        </div>

        <div class="ie-other-info" style="text-align: center;margin-top: 18px">
            <p style="line-height: 1;font-size: 13px;color:#9E9E9E">You may paste this link into your browser:</p>
            <a href="" style="text-decoration: none;color: #FF6D00;font-weight: 700;font-size: 12px">
                {{$activation_link or ""}}
            </a>

            <p>
                Your sign email is :<br/>
                <a href="mailto:{{$pt_user_email or "your email address"}}">{{$pt_user_email or "Your email address"}}</a>
            </p>

            <p>
                Your username is :
                <b>{{$pt_user_name or "**********"}}</b>
            </p>
        </div>


        <p style="font-size: 10px;font-weight: 700;"  class="ie-msg ie-warn">
            This email is auto generated. Do not respond to this mail.
        </p>
    </div>
</div>
<div>
    <img src="{{url().'/assets/images/image001.jpg'}}">
</div>
</body>
</html>