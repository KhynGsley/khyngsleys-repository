<!--/**
* @author Okeke Paul Ugochukwu
* @company VASCON Solutions
* @company VAS-CONSULTING
* @email pugochukwu@vas-consulting.com
* @alt-email donpaul120@gmail.com
* Date: 26/02/2016
* Time: 12:15
*/-->
<html>
<head>
</head>
    <body>
        {{--<h1 style="font-size: 25px;font-weight: 400;font-family: Tahoma" class="ie-greetings">Hi, {{$email or 'Customer'}}</h1>--}}
        <p style="font-size: 17px;font-weight: 300;font-family: Tahoma" class="ie-msg">
            Thank you for registering with Ikeja Electric. You can now log complaints, make requests, enquires, monitor
            resolution status and track your feedback.</p>

        <p style="font-size: 17px;font-weight: 300;font-family: Tahoma"  class="ie-msg">
            To proceed, click the link below :
            <br/>
            <a class="ie-link" href="http://199.189.84.76/ieportal/public/">continue</a>
        </p>

        <p style="font-size: 17px;font-weight: 300;font-family: Tahoma"  class="ie-msg">
            This email has been sent to {{$pt_user_email or 'you'}}.
            <br/>
            If you prefer not to receive emails of this type in future or you received this mail in error,
            <a class="ie-link" href="#">Unsubscribe here.</a>
        </p>

        <p style="font-size: 17px;font-weight: 500;font-family: Tahoma"  class="ie-msg ie-warn">
            This email is auto generated. Do not respond to this mail.
        </p>
    
    <div>
        <img src="{{url().'/assets/images/image001.jpg'}}">
    </div>
    </body>
</html>