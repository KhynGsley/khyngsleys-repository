<!--/**
* @author Okeke Paul Ugochukwu
* @company VASCON Solutions
* @company VAS-CONSULTING
* @email pugochukwu@vas-consulting.com
* @alt-email donpaul120@gmail.com
* Date: 26/02/2016
* Time: 12:15
*/-->
<html>
<head>
    <link href="{{url()}}/assets/images/ie_logo-min.png" rel="shortcut icon" type="image/x-icon" />
    <style>
        *{
            margin: 0;
            padding: 0;
            font-family: "Open Sans";
            overflow: hidden;
        }
        .ie-error-page-container{
            background: #fff;
            padding: 20px;
            width: 100%;
            height: 100%;
        }
        .ie-error-page-message{
            font-size: 90px;
            color: #616161;
            font-weight: 700;
            width: 50%;
            height: 100%;
            display: inline-block;
            background: #fff;
        }
        .ie-error-page-place{
            font-size: 80px;
            font-weight: 600;
            color: #FF9100;
        }
        .ie-error-page-wrong{
            font-size: 75px;
            font-weight: 500;
            color: #616161;
        }
        .ie-error-page-funny{
            font-size: 70px;
            font-weight: 400;
            color: #C62828;
        }

        .ie-error-code{
            font-size: 150px;
            color: #fff;
            font-weight:500;
            text-shadow: 2px 2px 2px rgba(0,0,0, 0.4);
        }
        .ie-error-page-divider{
            width: 50%;
            height: 100%;
            position: absolute;
            /*display: inline-block;*/
            right: -60px;
            top: 0;
            clear: both;
            transform: skewX(-8deg);
            background: #C62828;
            box-shadow: 3px 6px 4px rgba(0,0,0,0.9);
        }
        .ie-content-error{
            display: inline-block;
            float: right;
            z-index: 99999999;
            position: absolute;
            top: 20%;
            right: 15%;
        }
        .ie-error-page-divider:after{
            /*content: "";*/
            /*position: absolute;*/
            /*top: 0;*/
            /*right: -40px;*/
            /*height: 130%;*/
            /*width: 50%;*/
            /*transform: skewX(-8deg);*/
            /*background: #C62828;*/
            /*z-index: 2;*/
        }
        .ie-error-save-user{
            padding: 40px 0;
        }
        .ie-error-save-user button{
            font-size: 17px;
            font-weight: 400;
            color: #fff;
            text-decoration: none;
            position: relative;
            background: #5165E8 ;
            padding: 10px 15px;
            border-radius: 3px;
            border: none;
            outline: none;
            box-shadow: 2px 2px 4px rgba(17, 17, 17, 0.72);
            cursor: pointer;
        }
        .ie-error-save-user button:hover{
            background: #536DFE ;
            box-shadow: 2px 3px 4px rgba(17, 17, 17, 0.72);
        }
        .ie-error-save-user button:active{
            background: #2962FF;
            box-shadow: 2px 1px 4px rgba(17, 17, 17, 0.72);
        }

    </style>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:100,200,400,300,600,700&subset=all" rel="stylesheet"/>
</head>
<body>
    <div class="ie-error-page-container">
        <div class="ie-error-page-message">
            <span style="font-size: 90px;font-weight: 600;">Chai </span>!!!,<br/>
            <div class="ie-error-page-place">Where is this place?</div>
            <div class="ie-error-page-wrong">Something is wrong</div>
            <span class="ie-error-page-funny">If i got you here, I'm really sorry.</span>

            <div class="ie-error-save-user">
                <button onclick="helpOnError()" class="ie-help-on-error" href="">I can help you</button>
            </div>
        </div>
        <div class="ie-error-page-divider">

        </div>
        <script>
            function helpOnError(){
                window.location.href = "{{$help or "http://www.ikejaelectric.com"}}";
            }
        </script>
    </div>
    <div class="ie-content-error">
        <div><img src="{{url()}}/assets/images/my_smiley.png"></div>
        <div class="ie-error-code">404</div>
    </div>
</body>
</html>