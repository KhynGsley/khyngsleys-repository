<!--/**
* @author Okeke Paul Ugochukwu
* @company VASCON Solutions
* @company VAS-CONSULTING
* @email pugochukwu@vas-consulting.com
* @alt-email donpaul120@gmail.com
* Date: 26/02/2016
* Time: 12:15
*/-->
<!DOCTYPE html>
<html ng-app="application">
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{url()}}/public/assets/images/ie_logo-min.png" rel="shortcut icon" type="image/x-icon" />

    <script src="{{url()}}/build/js/ie.lib.main.js"></script>
    <script src="{{url()}}/build/js/ie.portal.min.js"></script>
    <title>IE Customer Portal</title>
</head>

<body>
<script>
    document.getElementsByTagName('body')[0].style.display = "none";
</script>
<div class="main">
    <div class="ie-overlay"></div>
    <div class="ie-portal-design">
        <div class="ie-index-page">
            <div class="ie-company-logo">
                <img height="70" width="60" src="{{url()}}/assets/images/ie_logo-min.png"/>
                <div class="ie-app-name">
                    <a>Ikeja Electric</a>
                    <span class="ie-app-type">Customer Portal</span>
                </div>
            </div>
            <div class="ie-portal-actions" style="padding: 40px 25px">
                <button id="ie-about-btn">ABOUT US</button>
                <button id="ie-corporate-btn">CORPORATE RESPONSIBILITY</button>
                <button id="ie-news-room-btn">NEWS ROOM</button>
                <button id="ie-faqs-btn" href="http://www.ikejaelectric.com/index.php/faq">FAQs</button>
                {{--<button id="ie-reg-btn">REGISTER</button>--}}
                {{--<button id="ie-login-btn">LOGIN</button>--}}
            </div>
        </div>

        <div class="ie-image-divider">
        </div>

        <div class="ie-user-reset-cont">
            <div class="ie-welcome-reset">
                Welcome <span class="ie-reset-user-name">{{$username}}</span>,
            </div>
            <div class="error">{{$error or ""}}</div>
            <div class="">
                <div class="ie-reset-title">You password reset was successful</div>
                {{--<div class='ie-reset-password_form-cont'>--}}
                    {{--<form action="{{$form_route}}" method="post">--}}
                        {{--<input type="hidden" name="zfc_mod_la_di" value="{{$user_id}}"/>--}}
                        {{--<input type="password" name="new_password" class="" placeholder="Enter New Password" required>--}}
                        {{--<input type="password" name="new_password_conf" class="" placeholder="Re-Enter New Password" required/>--}}
                        {{--<button type="submit" class="ie-general-btn">Submit</button>--}}
                    {{--</form>--}}
                {{--</div>--}}
            </div>
        </div>



        <div class="ie-footer" style="display: none;">
            <div class="ie-footer-item">
                <!--                    <img width="30" height="35" src="../public/assets/images/ie_logo.png">-->
                    <span class="ie-footer-item-name" style="font-weight: 600;margin-top: 4px">
                        Ikeja Electric
                    </span>
            </div>


            <div class="ie-footer-right">
                <div class="ie-footer-item">
                    <!--                        <img width="30" height="30" src="../public/assets/images/phone_icon.png">-->
                    <span class="ie-footer-item-name">
                        01-7000-250,
                        01-448-3900,
                        0700-022-5543
                    </span>
                </div>

                <div class="ie-footer-item">
                    <!--                        <img width="30" height="30" src="../public/assets/images/email_icon.png">-->
                    <span class="ie-footer-item-name">customercare@ikejaelectric.com</span>
                </div>
            </div>

        </div>
    </div>

</div>
</body>
<script async src="{{url()}}/build/js/utility/ie.portal.utility.js"></script>
<script>
    var lateLoading = function(){
        var lnk = document.createElement('link');
        lnk.href = "http://fonts.googleapis.com/css?family=Open+Sans:100,200,400,300,600,700&subset=all";
        lnk.rel = "stylesheet";
        var head = document.getElementsByTagName('head')[0];
        head.parentNode.insertBefore(lnk, head);

        var lnk2 = document.createElement('link');
        lnk2.href = '{{url()}}/'+"build/css/api.portal.css";
        lnk2.rel = "stylesheet";
        var head2 = document.getElementsByTagName('head')[0];
        head2.parentNode.insertBefore(lnk2, head2);
        document.getElementsByTagName('body')[0].style.display = "block";
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
            webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(lateLoading);
    else window.addEventListener('load', lateLoading);
</script>
<</html>