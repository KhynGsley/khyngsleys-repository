
<html xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:w="urn:schemas-microsoft-com:office:word"
      xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 14">
    <meta name=Originator content="Microsoft Word 14">
    <link rel=File-List href="CUSTOMER%20PORTAL%20DISCLAIMER_files/filelist.xml">
    <link rel=Edit-Time-Data
          href="CUSTOMER%20PORTAL%20DISCLAIMER_files/editdata.mso">
    <link href="../public/assets/images/ie_logo-min.png" rel="shortcut icon" type="image/x-icon" />
    <title>IE Customer Portal</title>
    <!--[if !mso]>
    <style>
        v\:* {behavior:url(#default#VML);}
        o\:* {behavior:url(#default#VML);}
        w\:* {behavior:url(#default#VML);}
        .shape {behavior:url(#default#VML);}
    </style>
    <![endif]--><!--[if gte mso 9]><xml>
    <o:DocumentProperties>
        <o:Author>Olumide Akinbolajo</o:Author>
        <o:Template>Normal</o:Template>
        <o:LastAuthor>paul okeke</o:LastAuthor>
        <o:Revision>2</o:Revision>
        <o:TotalTime>55</o:TotalTime>
        <o:Created>2016-02-12T18:35:00Z</o:Created>
        <o:LastSaved>2016-02-12T18:35:00Z</o:LastSaved>
        <o:Pages>4</o:Pages>
        <o:Words>1235</o:Words>
        <o:Characters>7041</o:Characters>
        <o:Company>Microsoft</o:Company>
        <o:Lines>58</o:Lines>
        <o:Paragraphs>16</o:Paragraphs>
        <o:CharactersWithSpaces>8260</o:CharactersWithSpaces>
        <o:Version>14.00</o:Version>
    </o:DocumentProperties>
    <o:OfficeDocumentSettings>
        <o:AllowPNG/>
    </o:OfficeDocumentSettings>
</xml><![endif]-->
    <link rel=themeData href="CUSTOMER%20PORTAL%20DISCLAIMER_files/themedata.thmx">
    <link rel=colorSchemeMapping
          href="CUSTOMER%20PORTAL%20DISCLAIMER_files/colorschememapping.xml">
    <!--[if gte mso 9]><xml>
    <w:WordDocument>
        <w:SpellingState>Clean</w:SpellingState>
        <w:GrammarState>Clean</w:GrammarState>
        <w:TrackMoves>false</w:TrackMoves>
        <w:TrackFormatting/>
        <w:PunctuationKerning/>
        <w:ValidateAgainstSchemas/>
        <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
        <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
        <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
        <w:DoNotPromoteQF/>
        <w:LidThemeOther>EN-GB</w:LidThemeOther>
        <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
        <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
        <w:Compatibility>
            <w:BreakWrappedTables/>
            <w:SnapToGridInCell/>
            <w:WrapTextWithPunct/>
            <w:UseAsianBreakRules/>
            <w:DontGrowAutofit/>
            <w:DontUseIndentAsNumberingTabStop/>
            <w:FELineBreak11/>
            <w:WW11IndentRules/>
            <w:DontAutofitConstrainedTables/>
            <w:AutofitLikeWW11/>
            <w:HangulWidthLikeWW11/>
            <w:UseNormalStyleForList/>
            <w:DontVertAlignCellWithSp/>
            <w:DontBreakConstrainedForcedTables/>
            <w:DontVertAlignInTxbx/>
            <w:Word11KerningPairs/>
            <w:CachedColBalance/>
        </w:Compatibility>
        <m:mathPr>
            <m:mathFont m:val="Cambria Math"/>
            <m:brkBin m:val="before"/>
            <m:brkBinSub m:val="&#45;-"/>
            <m:smallFrac m:val="off"/>
            <m:dispDef/>
            <m:lMargin m:val="0"/>
            <m:rMargin m:val="0"/>
            <m:defJc m:val="centerGroup"/>
            <m:wrapIndent m:val="1440"/>
            <m:intLim m:val="subSup"/>
            <m:naryLim m:val="undOvr"/>
        </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
    <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
                    DefSemiHidden="true" DefQFormat="false" DefPriority="99"
                    LatentStyleCount="267">
        <w:LsdException Locked="false" Priority="0" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
        <w:LsdException Locked="false" Priority="9" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
        <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
        <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
        <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
        <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
        <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
        <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
        <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
        <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
        <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
        <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
        <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
        <w:LsdException Locked="false" Priority="10" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Title"/>
        <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
        <w:LsdException Locked="false" Priority="11" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
        <w:LsdException Locked="false" Priority="22" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
        <w:LsdException Locked="false" Priority="20" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
        <w:LsdException Locked="false" Priority="59" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Table Grid"/>
        <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
        <w:LsdException Locked="false" Priority="1" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
        <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Shading"/>
        <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light List"/>
        <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Grid"/>
        <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 1"/>
        <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 2"/>
        <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 1"/>
        <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 2"/>
        <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 1"/>
        <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 2"/>
        <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 3"/>
        <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Dark List"/>
        <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Shading"/>
        <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful List"/>
        <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Grid"/>
        <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
        <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light List Accent 1"/>
        <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
        <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
        <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
        <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
        <w:LsdException Locked="false" Priority="34" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
        <w:LsdException Locked="false" Priority="29" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
        <w:LsdException Locked="false" Priority="30" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
        <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
        <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
        <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
        <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Dark List Accent 1"/>
        <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
        <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
        <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
        <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
        <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light List Accent 2"/>
        <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
        <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
        <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
        <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
        <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
        <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
        <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Dark List Accent 2"/>
        <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
        <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
        <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
        <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
        <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light List Accent 3"/>
        <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
        <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
        <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
        <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
        <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
        <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
        <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Dark List Accent 3"/>
        <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
        <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
        <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
        <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
        <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light List Accent 4"/>
        <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
        <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
        <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
        <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
        <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
        <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
        <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Dark List Accent 4"/>
        <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
        <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
        <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
        <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
        <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light List Accent 5"/>
        <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
        <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
        <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
        <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
        <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
        <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
        <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Dark List Accent 5"/>
        <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
        <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
        <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
        <w:LsdException Locked="false" Priority="60" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
        <w:LsdException Locked="false" Priority="61" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light List Accent 6"/>
        <w:LsdException Locked="false" Priority="62" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
        <w:LsdException Locked="false" Priority="63" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
        <w:LsdException Locked="false" Priority="64" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="65" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
        <w:LsdException Locked="false" Priority="66" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="67" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
        <w:LsdException Locked="false" Priority="68" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
        <w:LsdException Locked="false" Priority="69" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
        <w:LsdException Locked="false" Priority="70" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Dark List Accent 6"/>
        <w:LsdException Locked="false" Priority="71" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
        <w:LsdException Locked="false" Priority="72" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
        <w:LsdException Locked="false" Priority="73" SemiHidden="false"
                        UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
        <w:LsdException Locked="false" Priority="19" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
        <w:LsdException Locked="false" Priority="21" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
        <w:LsdException Locked="false" Priority="31" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
        <w:LsdException Locked="false" Priority="32" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
        <w:LsdException Locked="false" Priority="33" SemiHidden="false"
                        UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
        <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
        <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
    </w:LatentStyles>
</xml><![endif]-->
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:Calibri;
            panose-1:2 15 5 2 2 2 4 3 2 4;
            mso-font-charset:0;
            mso-generic-font-family:swiss;
            mso-font-pitch:variable;
            mso-font-signature:-536870145 1073786111 1 0 415 0;}
        @font-face
        {font-family:Tahoma;
            panose-1:2 11 6 4 3 5 4 4 2 4;
            mso-font-charset:0;
            mso-generic-font-family:swiss;
            mso-font-pitch:variable;
            mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
        @font-face
        {font-family:"Trebuchet MS";
            panose-1:2 11 6 3 2 2 2 2 2 4;
            mso-font-charset:0;
            mso-generic-font-family:swiss;
            mso-font-pitch:variable;
            mso-font-signature:1671 0 0 0 159 0;}
        @font-face
        {font-family:Verdana;
            panose-1:2 11 6 4 3 5 4 4 2 4;
            mso-font-charset:0;
            mso-generic-font-family:swiss;
            mso-font-pitch:variable;
            mso-font-signature:-1593833729 1073750107 16 0 415 0;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {mso-style-unhide:no;
            mso-style-qformat:yes;
            mso-style-parent:"";
            margin-top:0cm;
            margin-right:0cm;
            margin-bottom:10.0pt;
            margin-left:0cm;
            line-height:115%;
            mso-pagination:widow-orphan;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";
            mso-fareast-font-family:Calibri;
            mso-bidi-font-family:"Times New Roman";
            mso-ansi-language:EN-US;
            mso-fareast-language:EN-US;}
        a:link, span.MsoHyperlink
        {mso-style-priority:99;
            mso-style-parent:"";
            color:#0563C1;
            text-decoration:underline;
            text-underline:single;}
        a:visited, span.MsoHyperlinkFollowed
        {mso-style-noshow:yes;
            mso-style-priority:99;
            color:purple;
            mso-themecolor:followedhyperlink;
            text-decoration:underline;
            text-underline:single;}
        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
        {mso-style-noshow:yes;
            mso-style-priority:99;
            mso-style-link:"Balloon Text Char";
            margin:0cm;
            margin-bottom:.0001pt;
            mso-pagination:widow-orphan;
            font-size:8.0pt;
            font-family:"Tahoma","sans-serif";
            mso-fareast-font-family:Calibri;
            mso-ansi-language:EN-US;
            mso-fareast-language:EN-US;}
        p.seqpara1, li.seqpara1, div.seqpara1
        {mso-style-name:seqpara1;
            mso-style-unhide:no;
            mso-style-qformat:yes;
            margin-top:0cm;
            margin-right:0cm;
            margin-bottom:8.0pt;
            margin-left:36.0pt;
            text-align:justify;
            text-indent:-36.0pt;
            line-height:107%;
            mso-pagination:widow-orphan;
            font-size:10.0pt;
            font-family:"Verdana","sans-serif";
            mso-fareast-font-family:Calibri;
            mso-bidi-font-family:"Times New Roman";
            mso-fareast-language:EN-US;}
        p.seqlist1, li.seqlist1, div.seqlist1
        {mso-style-name:seqlist1;
            mso-style-unhide:no;
            mso-style-qformat:yes;
            mso-style-parent:seqpara1;
            margin-top:0cm;
            margin-right:0cm;
            margin-bottom:8.0pt;
            margin-left:72.0pt;
            text-align:justify;
            text-indent:-36.0pt;
            line-height:107%;
            mso-pagination:widow-orphan;
            font-size:10.0pt;
            font-family:"Verdana","sans-serif";
            mso-fareast-font-family:Calibri;
            mso-bidi-font-family:"Times New Roman";
            mso-fareast-language:EN-US;}
        p.seqheading1, li.seqheading1, div.seqheading1
        {mso-style-name:seqheading1;
            mso-style-unhide:no;
            mso-style-qformat:yes;
            mso-style-parent:"";
            margin-top:0cm;
            margin-right:0cm;
            margin-bottom:10.0pt;
            margin-left:0cm;
            text-align:center;
            line-height:115%;
            mso-pagination:widow-orphan;
            font-size:11.0pt;
            mso-bidi-font-size:14.0pt;
            font-family:"Verdana","sans-serif";
            mso-fareast-font-family:Calibri;
            mso-bidi-font-family:"Times New Roman";
            mso-ansi-language:EN-US;
            mso-fareast-language:EN-US;
            font-weight:bold;
            mso-bidi-font-weight:normal;}
        p.seqpara2, li.seqpara2, div.seqpara2
        {mso-style-name:seqpara2;
            mso-style-unhide:no;
            mso-style-qformat:yes;
            mso-style-parent:seqlist1;
            margin-top:0cm;
            margin-right:0cm;
            margin-bottom:8.0pt;
            margin-left:0cm;
            text-align:justify;
            line-height:107%;
            mso-pagination:widow-orphan;
            font-size:10.0pt;
            font-family:"Verdana","sans-serif";
            mso-fareast-font-family:Calibri;
            mso-bidi-font-family:"Times New Roman";
            mso-fareast-language:EN-US;}
        span.BalloonTextChar
        {mso-style-name:"Balloon Text Char";
            mso-style-noshow:yes;
            mso-style-priority:99;
            mso-style-unhide:no;
            mso-style-locked:yes;
            mso-style-parent:"";
            mso-style-link:"Balloon Text";
            mso-ansi-font-size:8.0pt;
            mso-bidi-font-size:8.0pt;
            font-family:"Tahoma","sans-serif";
            mso-ascii-font-family:Tahoma;
            mso-fareast-font-family:Calibri;
            mso-hansi-font-family:Tahoma;
            mso-bidi-font-family:Tahoma;}
        span.GramE
        {mso-style-name:"";
            mso-gram-e:yes;}
        .MsoChpDefault
        {mso-style-type:export-only;
            mso-default-props:yes;
            mso-ascii-font-family:Calibri;
            mso-fareast-font-family:Calibri;
            mso-hansi-font-family:Calibri;}
        /* Page Definitions */
        @page
        {mso-footnote-separator:url("CUSTOMER%20PORTAL%20DISCLAIMER_files/header.htm") fs;
            mso-footnote-continuation-separator:url("CUSTOMER%20PORTAL%20DISCLAIMER_files/header.htm") fcs;
            mso-endnote-separator:url("CUSTOMER%20PORTAL%20DISCLAIMER_files/header.htm") es;
            mso-endnote-continuation-separator:url("CUSTOMER%20PORTAL%20DISCLAIMER_files/header.htm") ecs;}
        @page WordSection1
        {size:595.3pt 841.9pt;
            margin:54.0pt 3.0cm 70.85pt 3.0cm;
            mso-header-margin:35.4pt;
            mso-footer-margin:35.4pt;
            mso-paper-source:0;}
        div.WordSection1
        {page:WordSection1;}
        -->
    </style>
    <!--[if gte mso 10]>
    <style>
        /* Style Definitions */
        table.MsoNormalTable
        {mso-style-name:"Table Normal";
            mso-tstyle-rowband-size:0;
            mso-tstyle-colband-size:0;
            mso-style-noshow:yes;
            mso-style-priority:99;
            mso-style-parent:"";
            mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
            mso-para-margin:0cm;
            mso-para-margin-bottom:.0001pt;
            mso-pagination:widow-orphan;
            font-size:10.0pt;
            font-family:"Calibri","sans-serif";}
    </style>
    <![endif]--><!--[if gte mso 9]><xml>
    <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
    <o:shapelayout v:ext="edit">
        <o:idmap v:ext="edit" data="1"/>
    </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-GB link="#0563C1" vlink=purple style='tab-interval:36.0pt'>

<div class=WordSection1>

    <p class=seqheading1><span lang=EN-US style='mso-no-proof:yes'><!--[if gte vml 1]><v:shapetype
            id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
            path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
        <v:stroke joinstyle="miter"/>
        <v:formulas>
            <v:f eqn="if lineDrawn pixelLineWidth 0"/>
            <v:f eqn="sum @0 1 0"/>
            <v:f eqn="sum 0 0 @1"/>
            <v:f eqn="prod @2 1 2"/>
            <v:f eqn="prod @3 21600 pixelWidth"/>
            <v:f eqn="prod @3 21600 pixelHeight"/>
            <v:f eqn="sum @0 0 1"/>
            <v:f eqn="prod @6 1 2"/>
            <v:f eqn="prod @7 21600 pixelWidth"/>
            <v:f eqn="sum @8 21600 0"/>
            <v:f eqn="prod @7 21600 pixelHeight"/>
            <v:f eqn="sum @10 21600 0"/>
        </v:formulas>
        <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
        <o:lock v:ext="edit" aspectratio="t"/>
    </v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_i1025" type="#_x0000_t75"
                           alt="Description: email sig" style='width:105.75pt;height:78pt;visibility:visible'>
        <v:imagedata src="../public/assets/images/image001.jpg" o:title="email sig"/>
    </v:shape><![endif]--><![if !vml]><img width=141 height=104
                                           src="../public/assets/images/image001.jpg"
                                           alt="Description: email sig" v:shapes="Picture_x0020_1"><![endif]></span><span
            lang=EN-US style='font-size:10.0pt;line-height:115%;font-family:"Trebuchet MS","sans-serif"'><o:p></o:p></span></p>

    <p class=seqheading1><span lang=EN-US style='font-size:22.0pt;line-height:115%;
font-family:"Trebuchet MS","sans-serif";color:#C00000'>CUSTOMER PORTAL
DISCLAIMER <o:p></o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>1.<span style='mso-tab-count:
1'>         </span>Introduction<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>1.1<span
            style='mso-tab-count:1'>       </span>This disclaimer shall govern your use of
our website/portal.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>1.2<span
            style='mso-tab-count:1'>       </span>By using our website/portal, you accept
this disclaimer in full; accordingly, if you disagree with this disclaimer or
any part of this disclaimer, you must not use our website/portal.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>1.3<span
            style='mso-tab-count:1'>       </span>Our website/Portal uses cookies; by using
our website/Portal or agreeing to this disclaimer, you consent to our use of
cookies in accordance with the terms of our [privacy and cookies policy].<o:p></o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>2.<span style='mso-tab-count:
1'>         </span>Credit<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>2.1<span
            style='mso-tab-count:1'>       </span>This document was created for Ikeja
Electric Customer Website/Portal<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>3.<span style='mso-tab-count:
1'>         </span>Copyright notice<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>3.1<span
            style='mso-tab-count:1'>       </span>Copyright (2016) <o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>3.2<span
            style='mso-tab-count:1'>       </span>Subject to the express provisions of this
disclaimer:<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(a)<span
            style='mso-tab-count:1'>        </span>we, together with our licensors, own and
control all the copyright and other intellectual property rights in our website/portal
and the material on our website/portal; and<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(b)<span
            style='mso-tab-count:1'>        </span>All the copyright and other intellectual
property rights in our website/portal and the material on our website/portal
are reserved.<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>4.<span style='mso-tab-count:
1'>         </span>Licence to use website/Portal<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>4.1<span
            style='mso-tab-count:1'>       </span>You may:<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(a)<span
            style='mso-tab-count:1'>        </span>View pages from our website in a web
browser;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(b)<span
            style='mso-tab-count:1'>        </span>Download pages from our website/portal
for caching in a web browser; and<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(c)<span
            style='mso-tab-count:1'>        </span>Print pages from our website/portal,<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><span
            style='mso-tab-count:1'>            </span>Subject to the other provisions of
this disclaimer<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><span
            style='mso-spacerun:yes'> </span>4.2<span style='mso-tab-count:1'>      </span>Except
as expressly permitted by Section 4.1 or the other provisions of this
disclaimer, you must not download any material from our website or save any
such material to your computer.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>4.3<span
            style='mso-tab-count:1'>       </span>You may only use our website for [your
own personal and business purposes], and you must not use our website for any
other purposes.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>4.4<span
            style='mso-tab-count:1'>       </span>Unless you own or control the relevant
rights in the material, you must not:<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(a)<span
            style='mso-tab-count:1'>        </span>Republish material from our website/portal
(including republication on another website);<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(b)<span
            style='mso-tab-count:1'>        </span>Sell, rent or sub-license material from
our website/portal;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(c)<span
            style='mso-tab-count:1'>        </span>Show any material from our website/portal
in public;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(d)<span
            style='mso-tab-count:1'>        </span>Exploit material from our website/portal
for a commercial purpose; or<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(e)<span
            style='mso-tab-count:1'>        </span>Redistribute material from our website/portal.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>4.5<span
            style='mso-tab-count:1'>       </span>We reserve the right to restrict access
to areas of our website, or indeed our whole website, at our discretion; you
must not circumvent or bypass, or attempt to circumvent or bypass, any access
restriction measures on our website.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>5.<span style='mso-tab-count:
1'>         </span>Acceptable use<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>5.1<span
            style='mso-tab-count:1'>       </span>You must not:<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(a)<span
            style='mso-tab-count:1'>        </span>Use our website/portal in any way or
take any action that causes, or may cause, damage to the website or impairment
of the performance, availability or accessibility of the website;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(b)<span
            style='mso-tab-count:1'>        </span>Use our website/portal in any way that
is unlawful, illegal, fraudulent or harmful, or in connection with any
unlawful, illegal, fraudulent or harmful purpose or activity;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(c)<span
            style='mso-tab-count:1'>        </span>Use our website/portal to copy, store,
host, transmit, send, use, publish or distribute any material which consists of
(or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke
logger, rootkit or other malicious computer software;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(d)<span
            style='mso-tab-count:1'>        </span>Conduct any systematic or automated data
collection activities (including without limitation scraping, data mining, data
extraction and data harvesting) on or in relation to our website without our
express written consent;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(e)<span
            style='mso-tab-count:1'>        </span>[Access or otherwise interact with our
website/Portal using any robot, spider or other automated means;]<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(f)<span
            style='mso-tab-count:1'>        </span>[violate the directives set out in the
robots.txt file for our website; or]<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(g)<span
            style='mso-tab-count:1'>        </span>[Use data collected from our website for
any direct marketing activity (including without limitation email marketing,
SMS marketing, telemarketing and direct mailing).]<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>5.2<span
            style='mso-tab-count:1'>       </span>You must not use data collected from our
website/Portal to contact individuals, companies or other persons or entities.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>5.3<span
            style='mso-tab-count:1'>       </span>You must ensure that all the information
you supply to us through our website, or in relation to our website/Portal, is
[true, accurate, current, complete and non-misleading].<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>6.<span style='mso-tab-count:
1'>         </span>Limited warranties<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>6.1<span
            style='mso-tab-count:1'>       </span>We do not warrant or represent:<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(a)<span
            style='mso-tab-count:1'>        </span>The completeness or accuracy of the
information published on our website/Portal;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(b)<span
            style='mso-tab-count:1'>        </span>That the material on the website/Portal
is up to date; or<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(c)<span
            style='mso-tab-count:1'>        </span>That the website/Portal or any service
on the website will remain available.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>6.2<span
            style='mso-tab-count:1'>       </span>We reserve the right to discontinue or
alter any or all of our website/portal services, and to stop publishing our
website/portal, at any time in our sole discretion without notice or
explanation; and save to the extent expressly provided otherwise in this
disclaimer, you will not be entitled to any compensation or other payment upon
the discontinuance or alteration of any website services, or if we stop
publishing the website/portal.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>6.3<span
            style='mso-tab-count:1'>       </span>To the maximum extent permitted by
applicable law and subject to Section 7.1, we exclude all representations and
warranties relating to the subject matter of this disclaimer, our website/portal
and the use of our website/portal.<o:p></o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>7.<span style='mso-tab-count:
1'>         </span>Limitations and exclusions of liability<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>7.1<span
            style='mso-tab-count:1'>       </span>Nothing in this disclaimer will:<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(a)<span
            style='mso-tab-count:1'>        </span>Limit or exclude any liability for death
or personal injury resulting from negligence;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(b)<span
            style='mso-tab-count:1'>        </span>Limit or exclude any liability for fraud
or fraudulent misrepresentation;<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(c)<span
            style='mso-tab-count:1'>        </span>Limit any liabilities in any way that is
not permitted under applicable law; or<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(d)<span
            style='mso-tab-count:1'>        </span>Exclude any liabilities that may not be
excluded under applicable law.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>7.2<span
            style='mso-tab-count:1'>       </span>The limitations and exclusions of
liability set out in this Section 7 and elsewhere in this disclaimer: <o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(a)<span
            style='mso-tab-count:1'>        </span><span class=GramE>are</span> subject to
Section 7.1; and<o:p></o:p></span></p>

    <p class=seqlist1><span style='font-family:"Trebuchet MS","sans-serif"'>(b)<span
            style='mso-tab-count:1'>        </span>Govern all liabilities arising under the
disclaimer or relating to the subject matter of the disclaimer, including
liabilities arising in contract, in tort (including negligence) and for breach
of statutory duty, except to the extent expressly provided otherwise in the
disclaimer.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>7.3<span
            style='mso-tab-count:1'>       </span>To the extent that our website/portal and
the information and services on our website/portal are provided free of charge,
we will not be liable for any loss or damage of any nature.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>7.4<span
            style='mso-tab-count:1'>       </span>We will not be liable to you in respect
of any losses arising out of any event or events beyond our reasonable control.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>7.5<span
            style='mso-tab-count:1'>       </span>We will not be liable to you in respect
of any business losses, including (without limitation) loss of or damage to
profits, income, revenue, use, production, anticipated savings, business,
contracts, commercial opportunities or goodwill.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>7.6<span
            style='mso-tab-count:1'>       </span>We will not be liable to you in respect
of any loss or corruption of any data, database or software.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>7.7<span
            style='mso-tab-count:1'>       </span>We will not be liable to you in respect
of any special, indirect or consequential loss or damage.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>7.8<span
            style='mso-tab-count:1'>       </span>You accept that we have an interest in
limiting the personal liability of our officers and employees and, having
regard to that interest, you acknowledge that we are a limited liability
entity; you agree that you will not bring any claim personally against our
officers or employees in respect of any losses you suffer in connection with
the website/portal or this disclaimer (this will not, of course, limit or
exclude the liability of the limited liability entity itself for the acts and
omissions of our officers and employees).<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>8.<span style='mso-tab-count:
1'>         </span>Variation<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>8.1<span
            style='mso-tab-count:1'>       </span>We may revise this disclaimer from time
to time.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>8.2<span
            style='mso-tab-count:1'>       </span>The revised disclaimer shall apply to the
use of our website/portal from the time of publication of the revised
disclaimer on the website/portal. <o:p></o:p></span></p>

    <p class=seqpara1 style='margin-left:0cm;text-indent:0cm'><b style='mso-bidi-font-weight:
normal'><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></b></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>9.<span style='mso-tab-count:
1'>         </span>Severability<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>9.1<span
            style='mso-tab-count:1'>       </span>If a provision of this disclaimer is determined
by any court or other competent authority to be unlawful and/or unenforceable,
the other provisions will continue in effect.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>9.2<span
            style='mso-tab-count:1'>       </span>If any unlawful and/or unenforceable
provision of this disclaimer would be lawful or enforceable if part of it were
deleted, that part will be deemed to be deleted, and the rest of the provision
will continue in effect. <o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>10.<span style='mso-tab-count:
1'>       </span>Law and jurisdiction<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>10.1<span
            style='mso-tab-count:1'>      </span>This disclaimer shall be governed by and
construed in accordance with Nigerian law.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>10.2<span
            style='mso-tab-count:1'>      </span>Any disputes relating to this disclaimer
shall be subject to the exclusive/non-exclusive jurisdiction of the courts of Nigeria.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'><o:p>&nbsp;</o:p></span></p>

    <p class=seqpara1><b style='mso-bidi-font-weight:normal'><span
            style='font-family:"Trebuchet MS","sans-serif"'>11.<span style='mso-tab-count:
1'>       </span>Our details<o:p></o:p></span></b></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>11.1<span
            style='mso-tab-count:1'>      </span>This website is owned and operated by <i
            style='mso-bidi-font-style:normal'>Ikeja Electric</i>.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>11.2<span
            style='mso-tab-count:1'>      </span>We are registered in [Nigeria] <a
            name="OLE_LINK4"></a><a name="OLE_LINK3"><span style='mso-bookmark:OLE_LINK4'>and
our registered office is at Obafemi Awolowo way Alausa, Ikeja, Lagos, Nigeria.</span></a><o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>11.3<span
            style='mso-tab-count:1'>      </span>Our principal place of business is at <a
            name="OLE_LINK6"></a><a name="OLE_LINK5"><span style='mso-bookmark:OLE_LINK6'>Obafemi
Awolowo way</span></a>, Alausa, Ikeja, <span class=GramE>Lagos</span>, Nigeria.<o:p></o:p></span></p>

    <p class=seqpara1><span style='font-family:"Trebuchet MS","sans-serif"'>11.4<span
            style='mso-tab-count:1'>      </span>You can contact us by writing to the
business address given above, by using our website contact form, by email to <span
                style='color:#7030A0'>customercare@ikejaelectric.com</span> or by telephone on
[01-7000250, 01-4483900, <span class=GramE>07000</span>-225-543].<i
                style='mso-bidi-font-style:normal'><o:p></o:p></i></span></p>

    <p class=MsoNormal><span lang=EN-US><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>
