<?php
/** ------------------------------------------------------------
 *  @Author Okeke Paul
 *  @Date 24/11/2015, Time: 11:43
 *  -------------------------------------------------------------
 *  @Description: Implemented all functions that interacts with
 *                the CRM.
 *
 * @func getContactCases
 * @func getContactRecentCases
 * @func getUserDetails
 * @func editUserDetails
 * @func getCaseNotesAndAttachment
 * @func downloadAttachment
 * @func addCase
 * @func getSingleCase
 * @func editCase
 * @func addNoteAndAttachment
 * @func changeProfilePicture
 * @func getProfilePicture
 *
 *  -------------------------------------------------------------
 *  @Copyright 2015, VASCON Solutions.
 *  -------------------------------------------------------------
 *  Collaborator :
 *  Description : ${Describe what you've done...}
 *  Date :
 * ------------------------------------------------------------ */

namespace App\Modules\CRM\Http\Controllers;
use App\Modules\Authentication\Repository\UserPortalRepository;
use App\Modules\CRM\Validator\CRMFormValidator;
use Asakusuma\SugarWrapper\Rest;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Laravel\Lumen\Routing\Controller as BaseController;


class CRMController extends BaseController
{

    private $fValidator;
    private $userPortal;
    private $crm;

    public function __construct(CRMFormValidator $formValidator, UserPortalRepository $userPortalRepository){
        $this->fValidator = $formValidator;
        $this->userPortal = $userPortalRepository;
        $this->crm = new Rest();
        if(!$this->crm->is_logged_in()) {
            $this->crm->setUrl('http://199.189.84.76/power/service/v4/rest.php');
            $this->crm->setUsername('paulex');
            $this->crm->setPassword('Nigeriasns$1');
            $this->crm->connect();
        }
    }


    /**
     * Retrieves all cases that belongs to a particular account/customer
     * @param $userID CRMController -> This is the crm account(customer) id
     * @return \Symfony\Component\HttpFoundation\Response
     * @since 2015
     * @author Paul Okeke
     */
    public function getContactCases($userID){
        $id = $userID;
        $cases = array();
        $fields = array(
            'Accounts'=>array('id','name'),
            'Cases'=>get_cases_columns(),
        );
        $options = [
            'where'=>'accounts.id="'.$id.'"',
        ];
        $result = $this->crm->get_with_related('Accounts', $fields, $options);
        if($result['result_count']>0 && sizeof($result['relationship_list'])>0) {
            $linkList = $result['relationship_list'][0]['link_list'];
            if (sizeof($linkList) > 0) {
                $records = $linkList[0]['records'];
                foreach ($records as $key => $record) {
                    $entryPoint = $record['link_value'];
                    $cases[] = array(
                        get_case_number($entryPoint['case_number']['value'], $entryPoint),
                        $entryPoint['name']['value'],
                        get_category_values($entryPoint['cstm_complaint_category_c']['value']),
                        get_sub_cat_values($entryPoint['cstm_complaint_sub_category_c']['value']),
                        get_case_priority($entryPoint['priority']['value']),
                        get_case_status($entryPoint['status']['value']),
                        get_case_date($entryPoint['date_entered']['value']),
                    );
                }
            }
        }
        $response['data'] = $cases;
        return response()->json($response);
    }

    /**
     * @param $userID CRMController -> This is the crm account(customer) id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getContactRecentCases($userID){
        $id = $userID;
        $cases = array();
        $fields = array(
            'Accounts'=>array('id','name'),
            'Cases'=>get_cases_columns(),
        );
        $options = array(
            'where'=>'accounts.id="'.$id.'"',
        );
        $result = $this->crm->get_with_related('Accounts', $fields, $options);
        if($result['result_count']>0 && sizeof($result['relationship_list'])>0) {
            $linkList = $result['relationship_list'][0]['link_list'];
            if (sizeof($linkList) > 0) {
                $records = $linkList[0]['records'];
                foreach ($records as $key => $record) {
                    $entryPoint = $record['link_value'];
                    $cases[] = array(
                        $entryPoint['case_number']['value'],
                        $entryPoint['name']['value'],
                        $entryPoint['cstm_complaint_category_c']['value'],
                        $entryPoint['cstm_complaint_sub_category_c']['value'],
                        get_case_priority($entryPoint['priority']['value']),
                        $entryPoint['status']['value'],
                        $entryPoint['description']['value'],
                        get_case_date($entryPoint['date_entered']['value']),
                    );
                }
            }
        }
        $caseNumbers = array();

        foreach($cases as $key=> $row){
            $caseNumbers[$key] = $row[0];
        }
        array_multisort($caseNumbers, SORT_DESC, $cases);

        $response = generate_response_success($cases);
        return response()->json($response);
    }

    function getUserDetails($userId){
        $id = $userId;
        $fields = array();
        $result = null;
        try {
            $result = $this->crm->get_by_id($id, 'Accounts', $fields);
        }catch (\Exception $ex){
            $response = generate_response_error('rolland');
            return response()->json($response);
        }
        if(isset($result[0]['warning'])){
            $response = generate_response_error($result[0]);
            return response()->json($response);
        }else{
            $response = generate_response_success($result[0]);
            return response()->json($response);
        }
    }

    function editUserDetails(Request $request, $userID){
        $v = $this->fValidator->validateUserForm($request);
        if($v->fails()){
            $response = generate_response_error($v->messages());
            return response()->json($response);
        }
        if(!$this->crm->is_valid_id($userID)){
            $response = generate_response_error("Invalid Account ID");
            return response()->json($response);
        }
        $data = array_merge(['id'=>$userID],$request->all());
        $result = $this->crm->set("Accounts", $data);
        if(!isset($result['id'])){
            $response = generate_response_error($result);
            return response()->json($response);
        }
        $response = generate_response_success($result);
        return response()->json($response);
    }

    /**
     * Use this function to get the details of a case note
     * @param $caseID
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function getCaseNotesAndAttachment($caseID){
        $id = $caseID;
        $notes = array();
        $fields = array(
            'Cases'=>array('id', 'name'),
            'Notes'=>array('id','name','description','filename','parent_id','parent_type','created_by')
        );
        $options = array(
            'where'=>'cases.id="'.$id.'"',
        );
        $result = $this->crm->get_with_related('Cases', $fields, $options);
        if($result['result_count']>0 && sizeof($result['relationship_list'])>0) {
            $linkList = $result['relationship_list'][0]['link_list'];
            if (sizeof($linkList) > 0) {
                $records = $linkList[0]['records'];
//                dd($records);
                foreach ($records as $key => $record) {
                    $entryPoint = $record['link_value'];
                    if(isset($entryPoint['filename']['value'])){
                        $entryPoint['note']['download'] = route('case.download.attachment', ['nid'=>$entryPoint['id']['value']]);
                    }
                    $notes[] = array(
                        $entryPoint['id']['name'] => $entryPoint['id']['value'],
                        $entryPoint['name']['name'] => $entryPoint['name']['value'],
                        $entryPoint['description']['name'] => html_entity_decode($entryPoint['description']['value']),
                        $entryPoint['filename']['name'] => $entryPoint['filename']['value'],
                        $entryPoint['created_by']['name'] => $entryPoint['created_by']['value'],
                        $entryPoint['parent_id']['name'] => $entryPoint['parent_id']['value'],
                        $entryPoint['parent_type']['name'] => $entryPoint['parent_type']['value'],
                        'note_download'=>$entryPoint['note']['download'],
                    );
                }
            }
        }
        $response = generate_response_success($notes);
        return response()->json($response);
    }

    function downloadAttachment(Request $request, $noteID){
        if(!isset($noteID)){
            $response = generate_response_error('specify the note id');
            return response()->json($response);
        }
        $result =  $this->crm->get_note_attachment($noteID);
        $filename = $result['note_attachment']['filename'];
        $file = $result['note_attachment']['file'];
        $file = base64_decode($file);
        return response($file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description','File Transfer')
            ->header('Content-disposition','attachment; filename='.$filename)
            ->header("Content-Type", "application/vnd.ms-excel")
            ->header('Content-Transfer-Encoding','binary')
            ->header('Content-Length', strlen($file));
    }

    /**
     * Function for adding a new case to the Sugar Crm
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function addCase(Request $request){
        $v = $this->fValidator->validateCase($request);
        if($v->fails()){
            $response = generate_response_error($v->messages());
            return response()->json($response);
        }
        if(!$this->crm->is_valid_id($request->get('account_id'))){
            $response = generate_response_error("Invalid Account ID");
            return response()->json($response);
        }
        $data = $request->all();
        $data['assigned_user_id']= '18d2347f-d1e7-3f50-7e69-569540a27a64';//this id maps to the customer portal user id
        $result = $this->crm->set("Cases", $data);
        if(!isset($result['id'])){
            $response = generate_response_error($result);
            return response()->json($response);
        }
        Mail::send('CRM::template.email.complaint_created', $data, function($msg) use ($data, $request){
            $portalUser = $this->userPortal->findUserByAccountNo($data['account_no']);
            if(sizeof($portalUser)>0){
                $portalUser = $portalUser[0];
                $msg->to($portalUser->pt_user_email, 'subject')->subject('Complaint Logged');
            }
        });
        $response = generate_response_success($result);
        return response()->json($response);
    }


    function getSingleCase($caseID){
        if(!$this->crm->is_valid_id($caseID)){
            $response = generate_response_error("Invalid ID");
            return response()->json($response);
        }
        $options = array(
            'where'=>'cases.id="'.$caseID.'"',
        );
        $fields = array();
        $result = $this->crm->get('Cases', $fields, $options);
        if(sizeof($result)>0){
            $result[0]['date_entered'] = get_case_date($result[0]['date_entered']);
            $result[0]['priority'] = get_case_priority($result[0]['priority']);
        }
        $response = generate_response_success($result);
        return response()->json($response);
    }

    /**
     * Use this function to edit a case
     * @param Request $request
     * @param $caseID String The id of the case
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function editCase(Request $request, $caseID){
        $v = $this->fValidator->validateCase($request);
        if($v->fails()){
            $response = generate_response_error($v->messages());
            return response()->json($response);
        }
        $valid = ($this->crm->is_valid_id($request->get('account_id'))&&
                 $this->crm->is_valid_id($caseID)
                );
        if(!$valid){
            $response = generate_response_error("Invalid ID");
            return response()->json($response);
        }
        $data = array_merge(["id"=>$caseID],$request->all());
        $result = $this->crm->set("Cases", $data);
        if(!isset($result['id'])){
            $response = generate_response_error($result);
            return response()->json($response);
        }
        $response = generate_response_success($result);
        return response()->json($response);
    }


    /**
     * This function is for adding a note to a the case
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function addNoteAndAttachment(Request $request){
        $v = $this->fValidator->validateNote($request);
        if($v->fails()){
            $response = generate_response_error($v->messages());
            return response()->json($response);
        }
        if(!$this->crm->is_valid_id($request->get('parent_id'))){
            $response = generate_response_error("Invalid ID");
            return response()->json($response);
        }
        $data = $request->all();
        $result = $this->crm->set("Notes", $data);
        if(!isset($result['id'])){
            $response = generate_response_error($result);
            return response()->json($response);
        }
        $noteId = $result['id'];
        if($request->has('file')){
           $result['file_details'] = $this->crm->set_note_attachment(
               $noteId,
               $request->get('file'),
               $request->get('filename')
           );
        }
        $response = generate_response_success($result);
        return response()->json($response);
    }


    public function changeProfilePicture(Request $request){
        $v = $this->fValidator->validatePictureChange($request);
        if($v->fails()){
            $response = generate_response_error($v->messages());
            return response()->json($response);
        }
        $user = $this->userPortal->findUserByAccountNo($request->get('account_no'));
        if(sizeof($user)<1){
            $response = generate_response_error("the user doesn't exist");
            return response()->json($response);
        }
        $user = $user[0];
        $content = base64_decode($request->get('user_image'));//$request->file('user_image');
        $seedValue = mt_rand(1, 200);
        $newImageName = $seedValue . '_' .$user->pt_user_acct_no.'_'.$user->pt_user_name.'.jpg';
        $prevName = $user->pt_user_image;
        try {
            Storage::put('profile_images' . '//' . $newImageName, $content);
            $user->pt_user_image = $newImageName;
            $user->save();
            $user->pt_user_image = route('portal.get.picture',['p'=>$newImageName]);
        }catch(\Exception $ex){
            return response($ex->getMessage(), 500);
        }
        if(isset($prevName) && strlen($prevName)>0){
            Storage::delete('profile_images' . '//' .$prevName);
        }
        unset($user['pt_user_pass']);
        $response = generate_response_success($user);
        return response()->json($response);
    }

    public function getProfilePicture(Request $request, $imageName){
        if(isset($imageName)){
            try {
                $file = Storage::get('profile_images' . '//' . $imageName);
                $responseType = array('Content-Type' => "image/jpeg");
                return response()->make($file, 200, $responseType);
            }catch(FileNotFoundException $fe){
                $response = generate_response_error(array("error"=>"File Not Found", "trace"=>$fe->getMessage()));
                return response()->json($response);
            }
        }else{
            $response = generate_response_error("specify the image name");
            return response()->json($response);
        }
    }
}