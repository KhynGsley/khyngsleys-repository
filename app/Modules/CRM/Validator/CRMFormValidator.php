<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 26/11/2015
 * Time: 15:36
 */

namespace App\Modules\CRM\Validator;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CRMFormValidator
{


    function validateUserForm(Request $request){
        return Validator::make($request->all(),
            array(
//                'id'=>'required',
//                'cstm_lastname_c'=>'required|max:11',
//                'cstm_firstname_c'=>'required|max:11',
                'phone_alternate'=>'required|max:16',
                'phone_office'=>'required|max:16',
                'billing_address_street'=>'required',
                'billing_address_city'=>'required',
                'email1'=>'required',
            ),
            array(
//                'id.required'=>'Invalid Request, validation failed',
                'billing_address_street.required'=>'The Street Address is required',
                'billing_address_city.required'=>'The City Address is required',
            ));
    }


    function validateNote(Request $request){
        return Validator::make($request->all(),
            array(
                'name'=>'required|max:20',
                'description'=>'required|max:255',
                'parent_id'=>'required|min:35|max:40',
                'parent_type'=>'required|max:15',
            ),
            array(
                'parent_id.required'=>'What are you trying to do, No Parent Reference',
                'parent_type.required'=>'This record must belong to a parent',
            ));
    }

    function validateCase(Request $request){
        return Validator::make($request->all(),
            array(
                'name'=>'required|max:40',
                'account_id'=>'required',
                'cstm_channel_type_c'=>'required|min:3|max:15',
                'priority'=>'required|max:4',
                'status'=>'required|max:10',
                'cstm_complaint_category_c'=>'required|min:2|max:30',
                'cstm_complaint_sub_category_c'=>'required|min:2|max:30',
                'description'=>'required|min:40|max:300',//the user needs to be more descriptive about the issue
            ),
            array(
                'cstm_complaint_category_c.required'=>'The Complaint Category is Required',
                'cstm_complaint_category_c.max'=>'The Complaint Category cannot be greater than 25 characters',
                'cstm_complaint_category_c.min'=>'The Complaint Sub Category cannot be less that 2 characters',
                'cstm_complaint_sub_category_c.required'=>'The Complaint Sub Category is Required',
                'account_id.required'=>'Please specify your account id',
                'cstm_channel_type_c.required'=>'You have no channel source'
            ));
    }

    function validatePictureChange(Request $request){
        return Validator::make($request->all(),
            array(
                'user_image'=>'required',
                'account_no'=>'required'
            ),
            array(
                'user_image.required'=>'The user image is required'
            )
            );
    }

}