<?php

namespace App\Modules\Authentication\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class BeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userToken = $request->header('vas-token');
        $account = $request->get('ie-acct-no');
        if(is_null($userToken) && is_null($account)){
            return response('Non-Authorized', 401)->header('WWW-Authenticate',
                'basic="My Realm" location="http://localhost:9010');
        }
        //first lets check that the token exist
        //lets check that the token belongs to this user
        $userTokens = DB::table('user_tokens')->where('usr_tk_token', $userToken)->get();
//        dd($userTokens);
        if(!sizeof($userTokens)>0){
            return response('Non-Authorized', 401)->header('WWW-Authenticate',
                'basic="My Realm" location="http://localhost:9010');
        }
        //TODO::check that the session hasn't expired
        return $next($request);
    }

    private function accessDenied()
    {
        $response = new AccessDeniedException('Access Denied');
        $type = array('reason'=>"ACCESS_DENIED", "failure_message"=>$response->getMessage());
        return generate_response_error($type);
    }
}
