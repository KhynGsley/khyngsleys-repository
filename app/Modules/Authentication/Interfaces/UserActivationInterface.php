<?php
/**
 * Created by PhpStorm.
 * User: paulex
 * Date: 25/02/16
 * Time: 09:18
 */

namespace App\Modules\Authentication\Interfaces;


interface UserActivationInterface
{
    function create(array $data);

    function findById($id);

    function findUserByToken($token);

    function findByUserId($token);

    function delete($id);
}