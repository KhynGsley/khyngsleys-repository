<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 23/04/2015
 * Time: 03:54
 */

namespace app\Modules\Authentication\Providers;


use Illuminate\Support\ServiceProvider;

class ModuleProvider extends ServiceProvider
{

    public function boot(){

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Modules\Authentication\Interfaces\UserTokenInterface',
            'App\Modules\Authentication\Repository\UserTokenRepository'
        );

        $this->app->bind(
            'App\Modules\Authentication\Interfaces\PortalUserInterface',
            'App\Modules\Authentication\Repository\UserPortalRepository'
        );

        $this->app->bind(
            'App\Modules\Authentication\Interfaces\UserActivationInterface',
            'App\Modules\Authentication\Repository\UserActivationRepository'
        );

        $this->app->bind(
            'App\Modules\Authentication\Interfaces\UserResetInterface',
            'App\Modules\Authentication\Repository\UserResetRepository'
        );

        $this->app->bind('CRMFields', function(){
           return null;
        });
    }
}