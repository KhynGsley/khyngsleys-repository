<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 07/12/2015
 * Time: 21:24
 */

namespace App\Modules\Authentication\Repository;


use App\Modules\Authentication\Interfaces\PortalUserInterface;
use App\Modules\Authentication\Model\PortalUser;

class UserPortalRepository implements PortalUserInterface
{

    private $model;

    public function __construct(PortalUser $model){
        $this->model = $model;
    }

    public function create(array $data)
    {
        $fill = array(
            'pt_user_acct_no'=>$data['account_number'],
            'pt_user_name'=>$data['cstm_portal_username_c'],
            'pt_user_pass'=>$data['cstm_portal_password_c'],
            'pt_user_email'=>$data['email1'],
        );
        return $this->model->create($fill);
    }

    public function edit(PortalUser $portalUser, array $data)
    {
        // TODO: Implement edit() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function findUserById($id)
    {
        $user = $this->model->where('pt_user_id', $id)->get();
        return $user;
    }

    /**
     * @param $acctNo
     * @return mixed
     */
    public function findUserByAccountNo($acctNo)
    {
       return $this->model->where('pt_user_acct_no', $acctNo)->get();
    }

    public function findUserByEmail($emailAddress)
    {
        return $this->model->where('pt_user_email', $emailAddress)->get();
    }

    public function findByUserEmailAndAcctNo($email, $acctNo)
    {
        return $this->model->where('pt_user_email', $email)
            ->where('pt_user_acct_no', $acctNo)->get();
    }
}