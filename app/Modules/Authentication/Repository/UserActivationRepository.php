<?php
/**
 * Created by PhpStorm.
 * User: paulex
 * Date: 25/02/16
 * Time: 09:20
 */

namespace App\Modules\Authentication\Repository;


use App\Modules\Authentication\Interfaces\UserActivationInterface;
use App\Modules\Authentication\Model\UserActivation;

class UserActivationRepository implements UserActivationInterface
{

    private $model;

    function __construct(UserActivation $model)
    {
        $this->model = $model;
    }

    function create(array $data)
    {
        return $this->model->create($data);
    }

    function findUserByToken($token)
    {
       return  $this->model->where("uac_token", $token)->get();
    }

    function findByUserId($id)
    {
        return $this->model->where("pt_user_id", $id)->get();
    }

    function delete($id)
    {
        $model = $this->model->find($id);
        if($model!==null){
            $model->delete();
        }
    }

    function findById($id)
    {
        return $this->model->find($id);
    }
}