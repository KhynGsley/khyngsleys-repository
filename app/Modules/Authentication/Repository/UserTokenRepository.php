<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 07/12/2015
 * Time: 18:55
 */

namespace App\Modules\Authentication\Repository;


use App\Modules\Authentication\Interfaces\UserTokenInterface;
use App\Modules\Authentication\Model\UserToken;

class UserTokenRepository implements UserTokenInterface
{

    public function __construct(UserToken $model){
        $this->model = $model;
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function findByToken($token){
         $usrTk = $this->model->where('usr_tk_token', $token)->get();
        if(sizeof($usrTk)>0){
            $usrTk = $usrTk[0];
            return $usrTk;
        }
        return null;
    }

    public function edit()
    {

    }

    public function delete($id)
    {
        $model = $this->model->find($id);
        if($model!=null){
            $model->delete();
        }
    }
}