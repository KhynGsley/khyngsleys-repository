<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 07/12/2015
 * Time: 18:58
 */

namespace App\Modules\Authentication\Model;

use Illuminate\Database\Eloquent\Model;
class UserResetPassword extends Model
{

    protected $table = "users_reset_password";
    protected $fillable = ['urp_token','pt_user_id'];
    protected $primaryKey = 'urp_id';
}