<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 07/12/2015
 * Time: 18:58
 */

namespace App\Modules\Authentication\Model;

use Illuminate\Database\Eloquent\Model;
class UserActivation extends Model
{

    protected $table = "user_activation";
    protected $fillable = ['uac_token','pt_user_id'];
    protected $primaryKey = 'uac_id';
}