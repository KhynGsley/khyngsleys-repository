<?php

namespace App\Modules\DataDump\Model;
use Illuminate\Database\Eloquent\Model;

class DataDump extends Model
{
    protected $table = "data_dump";
}