<?php

namespace App\Modules\DataDump\Repository;

use App\Modules\DataDump\Interfaces\IEDataDumpInterface;
use App\Modules\DataDump\Model\DataDump;

class IEDataDumpRepository implements IEDataDumpInterface
{

    function __construct(DataDump $dataDump){
        $this->model = $dataDump;
    }

    function findAll()
    {
        return $this->model->all();
    }

    function findByAccountNumber()
    {

    }

    function getDataCount(){
        return $this->model->count();
    }


    function findBySkippingAndLimiting($skip, $limit)
    {
       return $this->model->skip($skip)->take($limit)->get();
    }
}