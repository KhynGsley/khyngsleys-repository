<?php

namespace App\Modules\DataDump\Interfaces;

interface IEDataDumpInterface
{
    function findAll();

    function findBySkippingAndLimiting($skip, $limit);

    function getDataCount();

    function findByAccountNumber();
}