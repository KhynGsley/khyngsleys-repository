<?php
/**
 * Created by PhpStorm.
 * User: paulex10
 * Date: 23/04/2015
 * Time: 03:54
 */

namespace app\Modules\DataDump\Providers;


use Illuminate\Support\ServiceProvider;

class ModuleProvider extends ServiceProvider
{

    public function boot(){

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Modules\DataDump\Interfaces\IEDataDumpInterface',
            'App\Modules\DataDump\Repository\IEDataDumpRepository'
        );
    }
}