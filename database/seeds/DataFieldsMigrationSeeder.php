<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;

class DataFieldsMigrationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataDump = app('App\Modules\DataDump\Interfaces\IEDataDumpInterface');
        $dataCount = $dataDump->getDataCount();
        $batchCount = round($dataCount/400);
        $currentPos = $dataCount;
        $this->command->info($batchCount);
        $authCtrl = App::make('App\Modules\Authentication\Http\Controllers\AuthenticationController');

        while($currentPos>0){
            $skipCount = $dataCount - $currentPos;
            $records = $dataDump->findBySkippingAndLimiting($skipCount, $batchCount);
            foreach($records as $key=>$record){
                $accountNumber = str_replace('-', '', $record->account_no);
                $this->command->info($accountNumber);
                $postData = array(
                    'account_number'=>$accountNumber,
                    'phone_number'=>'08139000000',
                    'email'=>'okeketn@yahio.dom'
                );
                $request = Request::create('http://localhost/ieportal/public/reg/confirm', 'POST', $postData);
                $result = $authCtrl->confirmAccount($request);
                $response = json_decode($result->getContent(), true);
                if($response['status']=='success'){
                    //it means this account exist so now we add the corresponding data
                    $id = $response['message']['id'];
                    $this->command->info("The Account Details will be updated with new data");
                    $request = Request::create('http://localhost/ieportal/public/reg/migrate/data', 'POST', $this->makePostData($record));
                    $this->command->info("Editing Account Details :".$accountNumber ." Name: ".$record->account_name);
                    $result = $authCtrl->migrateCRMData($request, $id);
                    $this->command->info("Edited Account : ". $result->getContent());
                }else {
                    $this->command->info("The user already exist on the CRM");
                }
            }
            $currentPos = $currentPos - $batchCount;
            sleep(10);
        }
    }

    function makePostData($record){
        return array(
            'cstm_tariff_class_c'=>strtolower($record['account_type']),
        );
    }
}
