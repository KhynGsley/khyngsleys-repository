<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_user', function (Blueprint $table) {
            $table->increments('pt_user_id');
            $table->string('pt_user_acct_no');
            $table->string('pt_user_name');
            $table->string('pt_user_pass');
            $table->string('pt_user_email');
            $table->integer('pt_user_activated')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portal_user');
    }
}
