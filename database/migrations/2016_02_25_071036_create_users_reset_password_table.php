<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersResetPasswordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_reset_password', function (Blueprint $table) {
            $table->increments('urp_id');
            $table->string('urp_token');
            $table->integer('pt_user_id')->unsigned();
            $table->foreign('pt_user_id')->references('pt_user_id')->on('portal_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_reset_password');
    }
}
